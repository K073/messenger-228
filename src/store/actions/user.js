import axios from '../../axios';
import * as actionTypes from './actionTypes';
import {push} from 'react-router-redux'

const fetchUsersDataRequest = () => {
  return {type: actionTypes.FETCH_USER_DATA_REQUEST}
};

const fetchUsersDataSuccess = data => {
  return {type: actionTypes.FETCH_USER_DATA_SUCCESS, data}
};

const fetchUsersDataError = error => {
  return {type: actionTypes.FETCH_USER_DATA_ERROR, error}
};

export const fetchUserData = () => {
  return dispatch => {
    dispatch(fetchUsersDataRequest());
    axios.get('/user')
      .then(response => {
        dispatch(fetchUsersDataSuccess(response.data));
        dispatch(push('/'))
      }, error => {
        dispatch(fetchUsersDataError(error));
      });
  }
};

const registerUserRequest = () => {
  return {type: actionTypes.REGISTER_USER_REQUEST}
};

const registerUserSuccess = data => {
  return {type: actionTypes.REGISTER_USER_SUCCESS, data}
};

const registerUserError = error => {
  return {type: actionTypes.REGISTER_USER_ERROR, error}
};

export const registerUser = (data) => {
  return dispatch => {
    dispatch(registerUserRequest());
    axios.post('/user', data)
      .then(response => {
        dispatch(registerUserSuccess(response.data));
      }, error => {
        dispatch(registerUserError(error));
      });
  }
};

export const userLogout = () => {
  return {type: actionTypes.USER_LOGOUT}
};