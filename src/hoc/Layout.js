import React from 'react';
import Header from "../components/Header/Header";
import {connect} from "react-redux";
import {Navbar} from "react-bootstrap";
import {userLogout} from "../store/actions/user";

class Layout extends React.Component {
  render() {
    return (
      <div style={{height: "100%"}}>
        <div style={{minHeight: "calc(100% - 70px)"}}>
          {this.props.auth ? <div style={{paddingTop: '70px'}}><Header user={this.props.user} logout={this.props.logout}/></div>: null}
          {this.props.children}
        </div>
        <Navbar inverse fluid staticTop style={{marginTop: '20px', marginBottom: '0'}}>
          <Navbar.Text>
            © LV 2018 все права защищены
          </Navbar.Text>
        </Navbar>
      </div>
    )
  }
}

const initMapStateToProps = state => {
  return {
    token: state.user.token,
    username: state.user.user.name,
    auth : state.user.auth,
    user: state.user.user
  }
};

const initMapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(userLogout())
  }
};

export default connect(initMapStateToProps, initMapDispatchToProps)(Layout);