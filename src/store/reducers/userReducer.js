import * as actionTypes from '../actions/actionTypes';

const initState = {
  auth: false,
  token: '',
  user: {}
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.REGISTER_USER_SUCCESS:
      return {...state, ...action.data};
    case actionTypes.FETCH_USER_DATA_SUCCESS:
      return {...state, ...action.data};
    case actionTypes.USER_LOGOUT:
      return {...state, auth: false, token: '', user: {}};
    default:
      return state;
  }
};

export default userReducer;