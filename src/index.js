// import React from 'react';
// import ReactDOM from 'react-dom';
// import {BrowserRouter} from 'react-router-dom';
// import {applyMiddleware, combineReducers, createStore} from 'redux';
// import {Provider} from 'react-redux';
// import thunkMiddleware from 'redux-thunk';
//
// import './index.css';
// import App from './containers/App/App';
// import registerServiceWorker from './registerServiceWorker';
//
// import userReducer from './store/reducers/userReducer';
// import configReducer from './store/reducers/configReducer';
//
// const rootReducer = combineReducers({
//   user: userReducer,
//   config: configReducer
// });
//
// const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));
//
// const app = (
//   <BrowserRouter>
//     <Provider store={store}>
//       <App/>
//     </Provider>
//   </BrowserRouter>
// );
//
// ReactDOM.render(app, document.getElementById('root'));
// registerServiceWorker();

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store, {history} from './store/configureStore';

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();