import axios from 'axios';

const state = JSON.parse(localStorage.getItem('state'));

const instance = axios.create({
  baseURL: 'http://69.64.59.252:8000',
  // baseURL: 'http://192.168.88.102:8000/',
  headers: {'x-access-token': state ? state.user.token : ''}
});

export default instance;