import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

class Header extends React.Component {
  render() {
    return (
      <Navbar collapseOnSelect staticTop fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <LinkContainer to={'/'}>
              <a>Chat</a>
            </LinkContainer>
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            <NavItem>
              Welcome {this.props.user.name}
            </NavItem>
            <NavItem  onClick={() => this.props.logout()}>Logout</NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default Header;