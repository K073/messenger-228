import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";


import Layout from "./hoc/Layout";
import PrivateRoute from "./hoc/PrivateRoute";
import Chat from "./containers/Chat/Chat";
import Login from "./containers/Login/Login";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <PrivateRoute path={'/'} exact component={Chat} />
          <Route path={'/login'} exact component={Login}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;