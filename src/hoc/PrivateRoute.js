import React from 'react';
import {Redirect, Route} from "react-router-dom";
import {connect} from "react-redux";

const PrivateRoute = ({component: Component, ...rest}) => {
  return (
    <Route {...rest} render={(props) => (
      rest.auth
        ? <Component {...props} />
        : <Redirect to={{
          pathname: '/login',
          state: {from: props.location}
        }}/>
    )}/>
  )
};

const initMapStateToProps = state => {
  return {
    auth: state.user.auth
  }
};

export default connect(initMapStateToProps, null)(PrivateRoute);