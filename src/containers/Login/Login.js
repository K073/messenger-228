import React from 'react';
import {Button, Col, ControlLabel, FormControl, FormGroup, Grid, HelpBlock, PageHeader, Row} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {connect} from "react-redux";
import {fetchUserData} from "../../store/actions/user";

class Login extends React.Component {
  state = {
    login: '',
    password: ''
  };

  inputHandler = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({[name]: value})
  };

  login = (event) => {
    event.preventDefault();
    this.props.login(this.state.login, this.state.password)
  };

  render() {
    return(
      <Grid>
        <Row>
          <Col lg={4} lgOffset={4} md={6} mdOffset={3}>
            <form onSubmit={this.login}>
              <PageHeader>Авторизация</PageHeader>
              <h4>Пожалуйста авторизуйтесь</h4>
              <FormGroup>
                <ControlLabel>Имя</ControlLabel>
                <FormControl
                  placeholder="Введите имя пользователя"
                  name="username"
                  type="text"
                  value={this.state.username}
                  onChange={this.inputHandler}
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Пароль</ControlLabel>
                <FormControl
                  placeholder="Введите пароль"
                  name="password"
                  type="password"
                  value={this.state.password}
                  onChange={this.inputHandler}
                />
                <HelpBlock><LinkContainer to={'/registration'}><a>Зарегестрироваться</a></LinkContainer></HelpBlock>
              </FormGroup>
              <Button bsStyle={'primary'} type={'submit'}>Войти</Button>
            </form>
          </Col>
        </Row>
      </Grid>
    )
  }
}

const initMapDispatchToProps = dispatch => {
  return {
    login: (email, password) => dispatch(fetchUserData(email, password))
  }
};

export default connect(null, initMapDispatchToProps)(Login);